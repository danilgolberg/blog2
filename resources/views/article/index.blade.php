@extends('layouts.app')

@section('content')
<div id="primary" class="content-area column full">
    <main id="main" class="site-main">
        <div class="grid portfoliogrid">
            @foreach($articles as $article)
            <article class="hentry">
                <header class="entry-header">
                    <div class="entry-thumbnail">
                        <a href="portfolio-item.html"><img src="/storage/{{$article->banner}}" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="p1" /></a>
                    </div>
                    <h2 class="entry-title">
                        <a href="{{route('article.edit', ["id" => $article->id])}}" rel="bookmark">{{$article->title}}</a>
                    </h2>
                    @foreach($article->tags as $tag)
                    <a class='portfoliotype' href='portfolio-category.html'>{{$tag->name}}</a>
                    @endforeach
                </header>
            </article>
            @endforeach


        </div>
        <!-- .grid -->

        <nav class="pagination">
            <span class="page-numbers current">1</span>
            <a class="page-numbers" href="#">2</a>
            <a class="next page-numbers" href="#">Next »</a>
        </nav>
        <br />

    </main>
    <!-- #main -->
</div>
<!-- #primary -->
@endsection()