@extends('layouts.app')

@section('content')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<article>


    <form method="POST" action="{{route('article.update')}}" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <input type="hidden" name="id" value="{{$article->id}}">
        <header class="entry-header">
            <h1 class="entry-title">Title</h1>
            @error('title')
            <div>{{ $message }}</div>
            @enderror
            <h1 class="entry-title">
                <input type="text" name="title" value="{{$article->title}}">
            </h1>
            <div class="entry-thumbnail">
                <a href="portfolio-item.html"><img src="/storage/{{$article->banner}}" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="p1" /></a>
            </div>
            <div class="entry-thumbnail">
                <h1 class="entry-title">Upload image</h1>
                <input type="file" name="banner">
            </div>
            @error('file')
            <div>{{ $message }}</div>
            @enderror
        </header>
        <!-- .entry-header -->
        <div class="entry-content">
            @error('content')
            <div>{{ $message }}</div>
            @enderror
            <textarea rows="6" name="content">{{$article->content}}</textarea>
        </div>
        <div class="entry-tags" style="display: flex; flex-direction:column">
            @foreach($tags as $tag)
            @error('tags')
            <div>{{ $message }}</div>
            @enderror
            <input type="checkbox" id="{{$tag->name}}" name="tags[]" value="{{$tag->id}}">
            <label for="{{$tag->name}}">{{$tag->name}}</label>
            @endforeach
        </div>
        <!-- .entry-footer -->
        <button class="wpcmsdev-button color-green hentry" type="submit">Create</button>



        <div class="entry-content">
            @error('content')
            <div>{{ $message }}</div>
            @enderror
            <h2 class="entry-title">
                <a href="{{route('article.delete', ["id" => $article->id])}}" rel="bookmark">Delete</a>
            </h2>

        </div>
    </form>

</article>
@endsection