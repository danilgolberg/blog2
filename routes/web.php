<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainPageController;
use App\Http\Controllers\ArticleController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [MainPageController::class, 'index']);
Route::middleware(['auth'])->group(function () {
    Route::prefix('/article')->name('article.')->group(function () {
        Route::get('/', [ArticleController::class, 'index'])->name('index');
        Route::get('/form', [ArticleController::class, 'create'])->name('create');
        Route::post('/', [ArticleController::class, 'store'])->name('store');
        Route::get('/edit/{id}', [ArticleController::class, 'edit'])->name('edit');
        Route::put('/', [ArticleController::class, 'update'])->name('update');
        Route::get('/edit/{id}/delete', [ArticleController::class, 'delete'])->name('delete');
    });
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});





require __DIR__ . '/auth.php';
