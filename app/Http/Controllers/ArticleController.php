<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models;
use Illuminate\Support\Facades\Auth;
use App\Models\Article;
use App\Models\Tag;
use Exception;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Log;
use App\Http\Requests\IncomingRequest;
use App\Http\Requests\EditIdRequest;
use App\Http\Requests\UpdateRequest;
use App\Http\Requests\DeleteRequest;



class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id = Auth::id();

        $articles = Article::with(('tags'))
            ->where('user_id', $id)
            ->simplePaginate(10);




        return  view('article.index', ['articles' => $articles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::all();

        return view('article.create', ['tags' => $tags]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(IncomingRequest $request, Article $article)
    {

        $user = Auth::user();
        try {
            DB::beginTransaction();
            $article->user_id = $user->id;
            $article->title = $request->title;
            $article->content = $request->content;
            $article->banner = $request->file('banner')->store('banners');
            $article->save();
            $article->tags()->sync($request->tags);
            DB::commit();
        } catch (Exception $ex) {
            //  Log::error($ex);
            DB::rollBack();
        }

        $validated = $request->validated();
        $validated = $request->safe()->only(['user_id', 'title', 'content', 'banner', 'tags',]);
        $validated = $request->safe()->except(['user_id', 'title', 'content', 'banner', 'tags',]);
        return $article;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(EditIdRequest $request, $id)
    {
        $tags = Tag::all();
        $article = Article::findOrFail($id);
        return view('article.edit', ['article' => $article], ['tags' => $tags]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request)
    {
        $article = Article::findOrFail($request->id);
        $userId = Auth::id();
        if ($article->user_id !== $userId) {
            abort(403);
        }
        if ($request->hasFile('banner')) {
            $path = $request->file('banner')->store('banners');
            $previosBanner = $article->banner;
            $article->banner = $path;
        }

        $article->title = $request->title;
        $article->content = $request->content;
        $article->tags()->sync($request->tags);
        $article->save();

        if (isset($previosBanner)) {
            Storage::delete($previosBanner);
        }
        return redirect(route('article.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(DeleteRequest $request)
    {
        $article = Article::findOrFail($request->id);
        $userId = Auth::id();
        if ($article->user_id !== $userId) {
            abort(403);
        }
        $article->delete();
        return redirect(route('article.index'));
    }
}
