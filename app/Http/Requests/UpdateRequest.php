<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [

                'unique:article,name,' . $this->id
            ],
            'title' => ['required', 'max:17', 'string'],
            'content' => ['required', 'max:125', 'string'],
            'banner' => ['image', 'max:3000'],
            'tags' => ['required'],
        ];
    }
}
